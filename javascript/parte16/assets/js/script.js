function iniciales(x, y, z) {
  console.log(x++, y++, z++);
}
function operacion() {
  a = 5;
  b = ++a;
  c = a++;
  b = b * 5;
  a = a * 2;
  console.log(a, b, c);
}
function algebra(x, y, z) {
  x = 15;
  y = -10;
  z = 214;
  for (let i = 0; i < 12; i++) {
    console.log(x++, y++, z++);
  }
}
function resultado() {
  a = 5;
  b = a + 2;
  b -= 3;
  c = -3;
  c *= 2;
  ++c;
  a *= b;
  console.log(a, b, c);
}
