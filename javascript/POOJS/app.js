/*$(function () {
    var buscar = [
        "Curso",
        "Nombre",
        "Plantilla",
        "Examenes",
        "JAVASCRIPT",
        "CSS",
        "HTML5",
        "FRAMEWORK",
    ];
    $(".tag").autocomplete({ source: buscar });
    $("#tag").html({source: buscar});
});*/
jQuery(document).ready(function () {
    $('.formulario_busqueda').busqueda();
  });
  (function($){
    $.fn.busqueda = function(options){
      var defaults = { // Opciones por defecto
        selectorCampo: 'input[type="text"]' // Campo de texto
        ,selectorBoton: 'input[type="image"]' // Botón buscar
        ,claseInactivo: 'inactivo' // Clase de campo de texto inactivo
        ,imagenBoton: 'imagen_buscador.png' // Imagen inicial del botón de búsqueda
        ,imagenBotonHover: 'imagen_buscador_hover.png' // Imagen de rollover en botón de búsqueda
      }
      var op = $.extend(defaults, options);
  
      return this.each(function(){
        var capa = $(this);
        var boton = capa.find(op.selectorBoton).eq(0);
        precargar(capa,boton);
        placeholder(capa);
      });
      function precargar (capa,boton) { // Precarga de la imagen de rollover para el botón de búsqueda
        var rutaInicial = boton.attr('src');
        var rutaRollover = rutaInicial.replace(op.imagenBoton,op.imagenBotonHover);
        var imagen = new Image();
        imagen.src = rutaRollover;
        rolloverBoton(capa,boton);
      }
      
      function rolloverBoton (capa,boton) {
        // Rollover en el evento hover (mouseover, mouseout)
        boton.hover(
          function () {
            var rutaInicial = boton.attr('src');
            var rutaFinal = rutaInicial.replace(op.imagenBoton,op.imagenBotonHover);
            boton.attr('src',rutaFinal)
          }
          ,function () {
            var rutaInicial = boton.attr('src');
            var rutaFinal = rutaInicial.replace(op.imagenBotonHover,op.imagenBoton);
            boton.attr('src',rutaFinal)
          }
        );
        
      }
      
      function placeholder (capa) {
        if (!Modernizr.input.placeholder) { // Si no existe soporte de atributo placeholder (requiere Modernizr: http://www.modernizr.com/)...
          var campo = capa.find(op.selectorCampo).eq(0);
          var texto = campo.attr('placeholder'); // Se recoge el valor del placeholder
          campo.attr('value',texto).addClass(op.claseInactivo); // Se añade la clase inactivo (de ese modo se puede dar por estilos otro color cuando está el texto predeterminado)
          campo.focus(function () {
            if ($(this).attr('value')==texto) { // si al entrar el foco en el campo de texto lo que hay es el texto predeterminado...
              $(this).attr('value','').removeClass(op.claseInactivo);
            }
          });
          campo.blur(function () {
            if ($(this).attr('value').length==0) { // si al salir el foco del campo de texto no contiene ningún texto...
              $(this).attr('value',texto).addClass(op.claseInactivo);
            }
          });
        }
      }
    }
  })(jQuery);